import React, {Component} from 'react';
import {ScrollView, Text, TextInput, View} from 'react-native';
import formStyles from '../styles/formStyle';
import UserInput from './userInput';

class InputField extends Component {
    render() {
        return (
            <UserInput title={this.props.title}>
                {this.props.editable ?
                    <TextInput
                        style={formStyles.editableInput}
                        onChangeText={value => this.props.onChange(value)}
                        value={this.props.value}
                    /> :
                    <Text
                        style={formStyles.input}>
                        {this.props.value}
                    </Text>
                }
            </UserInput>
        );
    }
}

export default InputField;
