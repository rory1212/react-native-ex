import React, {Component} from 'react';
import ColorPicker from './colorPicker';
import InputField from './inputField';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TextInput,
    Button,
    Dimensions,

} from 'react-native';

class EditCar extends Component {
    car = this.props.navigation.state.params.car;
    state = {
        company: this.car.company,
        name: this.car.name,
        color: this.car.color,
        description: this.car.description,
    };

    componentDidMount(): void {
        let car = this.props.navigation.state.params.car;
        this.setState({
            company: car.company,
            name: car.name,
            color: car.color,
            description: car.description,
        });

        this.saveCar = this.props.navigation.state.params.saveCar;
        this.deleteCar = this.props.navigation.state.params.deleteCar;
    }

    render() {
        return (
            <View>
                <ScrollView style={{height: '96%'}}>
                    <InputField title="Company" value={this.state.company} editable={false}/>
                    <InputField title="Name" value={this.state.name} editable={false}/>
                    <InputField title="Description" value={this.state.description} editable={true}
                                onChange={text => this.setState({description: text})}/>
                    <ColorPicker
                        title="Color:"
                        color={this.state.color}
                        onChange={text => {
                            this.setState({color: text});
                        }}/>
                </ScrollView>
                <View style={{flex: 2, flexDirection: 'row'}}>
                    <View style={{flex: 1, ...styles.button}}>
                        <Button title='Delete' color='red' raised={true} onPress={() => {
                            this.deleteCar();
                            this.props.navigation.goBack();
                        }}/>
                    </View>
                    <View style={{flex: 1, ...styles.button}}>
                        <Button title='Save' raised={true} onPress={() => {
                            this.saveCar(this.state.description, this.state.color);
                            this.props.navigation.goBack();
                        }}/>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    button: {
        marginHorizontal: 5
    }
});

export default EditCar;
