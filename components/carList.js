import React, {Component} from 'react';
import CarListItem from './carListItem';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button
} from 'react-native';

class CarList extends Component {
    state = {
        cars: [

        ]
    };

    componentDidMount(): void {
        let carsToAdd = [
            {
                id: 0,
                company: 'Maruti Suzuki S-Presso',
                name: 'My car',
                color: '#ffa500',
                description: 'This is a car'
            },
            {
                id: 1,
                company: 'Renault Kwid',
                name: 'My 2nd car',
                color: '#170eff',
                description: 'This Renault car'
            }
        ];

        let cars = this.state.cars;
        for (let i = 0; i < 10; i++) {
            let car = {...carsToAdd[i % carsToAdd.length]};
            car.id = i;
            car.name += ' ' + i;
            cars.push(car);
        }
        this.setState({cars})
    }

    saveCar = (id) => {
        return (description, color) => {
            let cars = this.state.cars;
            let car = cars[cars.map(c => c.id).indexOf(id)];
            car.description = description;
            car.color = color;
            this.setState({cars});
        }
    };

    deleteCar = (id) => {
        let cars = this.state.cars;
        cars.splice(cars.map(c => c.id).indexOf(id), 1);
        this.setState({cars});
    };

    render() {
        const {navigate} = this.props.navigation;
        return (
            <ScrollView
                contentInsetAdjustmentBehavior="automatic">
                {this.state.cars.map(car =>
                    (
                        <CarListItem car={car}
                                     key={car.id}
                                     navigate={navigate}
                                     saveCar={this.saveCar(car.id)}
                                     deleteCar={() => this.deleteCar(car.id)}/>
                    )
                )}
            </ScrollView>
        )
    }
}

export default CarList;
