import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import ColorPixelInput from './colorPixelInput';
import {rgbToHex, hexToRgb, lightOrDark} from '../utils/rbg';
import UserInput from './userInput';

class ColorPicker extends Component {

    state = {
        r: 0,
        g: 0,
        b: 0,
        color: '',
    };

    componentDidMount(): void {
        this.setState({...this.state, ...hexToRgb(this.props.color)}, () => this.updateColor());
    }

    updateColor = () => {
        let color = rgbToHex(
            this.state.r,
            this.state.g,
            this.state.b,
        );
        this.setState({
            color,
        });
        this.props.onChange(color);
    };

    render() {
        let color = this.state.color || '#fff';
        let textColor = lightOrDark(color) === 'dark' ? 'white' : 'black';

        return (
            <UserInput title={this.props.title}
                       nonTextTitle={
                           <Text style={
                               {
                                   ...styles.colorDisplay,
                                   color: textColor,
                                   backgroundColor: color,
                                   borderColor: textColor,
                               }}>
                               {this.state.color}
                           </Text>
                       }>
                <View style={{flex: 3, flexDirection: 'column'}}>
                    <ColorPixelInput text="R" value={this.state.r} style={{flex: 1}}
                                     onChange={r => {
                                         this.setState({r}, this.updateColor);
                                     }}
                                     baseColor="red"/>
                    <ColorPixelInput text="G" value={this.state.g} style={{flex: 1}}
                                     onChange={g => {
                                         this.setState({g}, this.updateColor);
                                     }}
                                     baseColor="green"/>
                    <ColorPixelInput text="B" value={this.state.b} style={{flex: 1}}
                                     onChange={b => {
                                         this.setState({b}, this.updateColor);
                                     }}
                                     baseColor="blue"/>
                </View>
            </UserInput>
        );
    }
}

const styles = StyleSheet.create({
    colorDisplay: {
        borderRadius: 10,
        margin: 4,
        padding: 4,
        fontSize: 14,
        textAlign: 'center',
        textAlignVertical: 'center',
        borderWidth: 1,
        top: -7,
    },
});

export default ColorPicker;
