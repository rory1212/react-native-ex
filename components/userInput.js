import React, {Component} from 'react';
import formStyles from '../styles/formStyle';
import {StyleSheet, Text, TextInput, View} from 'react-native';

class UserInput extends Component {
    render() {
        return (
            <View style={formStyles.field}>
                <View style={{...formStyles.labelContainer, flexDirection: 'row'}}>
                    <Text style={formStyles.label}>
                        {this.props.title}
                    </Text>
                    {
                        this.props.nonTextTitle ?
                            <View>
                                {this.props.nonTextTitle}
                            </View> :
                            null
                    }
                </View>
                {this.props.children}
            </View>
        );
    }
}

export default UserInput;
