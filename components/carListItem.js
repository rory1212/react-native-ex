import React, {Component} from 'react';
import {lightOrDark} from '../utils/rbg';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
} from 'react-native';

class CarListItem extends Component {
    render() {
        let carColor = this.props.car.color;
        let textColor = lightOrDark(carColor) === 'dark' ? 'white' : 'black';
        return (
            <Text title={this.props.car.name}
                  onPress={() => this.props.navigate('Edit', {
                      name: this.props.car.name,
                      car: this.props.car,
                      saveCar: this.props.saveCar,
                      deleteCar: this.props.deleteCar
                  })}
                  style={{backgroundColor: carColor, color: textColor, ...styles.textBox}}>
                {this.props.car.name}
            </Text>
        );
    }
}

const styles = StyleSheet.create({
    textBox: {
        padding: 10,
        fontSize: 24,
        shadowRadius: 2,
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowColor: '#000',
        shadowOpacity: 0.8,
        elevation: 4,
        marginHorizontal: 8,
        marginTop: 8,
        marginBottom: 2,
        borderRadius: 8
    },
});

export default CarListItem;
