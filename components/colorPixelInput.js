import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import formStyle from '../styles/formStyle';
import {Slider} from 'react-native-elements';

class ColorPixelInput extends Component {
    state = {
        value: 0,
    };

    componentDidMount(): void {
        this.updateValue(this.props.value, false);
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps.value !== this.props.value) {
            this.updateValue(this.props.value, false);
        }
    }

    updateValue = (text, toUpdate = true) => {
        let value = 0;
        if (!isNaN(Number(text))) {
            let num = Number(text);
            if (num < 0) {
                value = 0;
            } else if (num > 255) {
                value = 255;
            } else {
                value = num;
            }
        } else {
            value = 0;
        }

        if (toUpdate) {
            this.props.onChange(value);
        }
        this.setState({value: value});
    };

    render() {
        return (
            <View style={{flex: 20, flexDirection: 'row'}}>
                <Text style={{flex: 1, ...formStyle.title, textDecorationLine: 'none'}}>
                    {this.props.text}
                </Text>
                <View style={{flex: 1}}/>
                <Slider
                    style={{width: 200, height: 40, flex: 17}}
                    minimumValue={0}
                    maximumValue={255}
                    minimumTrackTintColor={this.props.baseColor}
                    maximumTrackTintColor="gray"
                    onValueChange={this.updateValue}
                    value={this.state.value}
                    thumbTintColor={this.props.baseColor}
                    step={1}
                />
                <View style={{flex: 1}}/>
            </View>
        );
    }
}

export default ColorPixelInput;
