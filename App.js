import React from 'react';
import CarList from './components/carList';
import EditCar from './components/editCar';


import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

const MainNavigator = createStackNavigator({
    Home: {screen: CarList, navigationOptions: {title: 'Cars'}},
    Edit: {
        screen: EditCar, navigationOptions: ({navigation}) => ({
            title: `Car: ${navigation.state.params.name}`,
        }),
    },
});

const App = createAppContainer(MainNavigator);

export default App;
