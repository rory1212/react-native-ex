import {StyleSheet} from 'react-native';

let field = {
    padding: 9,
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#b1b1b1',
    margin: 8,
    marginTop: 15,
    paddingTop: 14,
    paddingBottom: 7,
    borderStyle: 'solid'
};

let labelContainer = {
    display: 'flex',
    position: 'absolute',
    height: 32,
    marginTop: -15,
    backgroundColor: '#ffffff',
    paddingHorizontal: 5,
    paddingVertical: 2,
    left: 10
};

let label = {
    fontSize: 20,
    overflow: 'hidden',
    fontWeight: 'normal',
};

let input = {
    fontSize: 20,
    padding: 10,
};
let editableInput = {
    ...input,
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 8,
    margin: 5,
    backgroundColor: 'white'
};

let title = {
    fontSize: 24,
    textDecorationLine: 'underline',
    padding: 10,
};

const styles = StyleSheet.create({
    field,
    input,
    editableInput,
    label,
    title,
    labelContainer
});

export default styles;
